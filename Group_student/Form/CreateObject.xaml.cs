﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Npgsql;

namespace Group_student.Form
{
    /// <summary>
    /// Логика взаимодействия для CreateObject.xaml
    /// </summary>
    public partial class CreateObject : Window
    {
        public CreateObject()
        {
            InitializeComponent();
        }

        private void AddObject_Click(object sender, RoutedEventArgs e)
        {
            NpgsqlCommand regist = new NpgsqlCommand();
            
            regist = new NpgsqlCommand("insert into Object( Name_Object, Teacher ) VALUES ('" + TextObject.Text + "','" + TextTeacher.Text + "')", MainWindow.connect);
            regist.ExecuteNonQuery();
        }
    }
}
