﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Data;
using Npgsql;
using Microsoft.Win32;
using System.IO;

namespace Group_student.Form
{
    /// <summary>
    /// Логика взаимодействия для Table.xaml
    /// </summary>
    public partial class Table : Window
    {

        State state;
        public Table()
        {
            InitializeComponent();
            // ТУТ Ё

            // Скрыть
            //AddButton.Visibility = Visibility.Hidden;
            DeleteObject.Visibility = Visibility.Hidden;
            AddStudent.Visibility = Visibility.Hidden;

            if (MainWindow.roleName == "student")
            {
                Logs.Visibility = Visibility.Collapsed;
                Teacher.Visibility = Visibility.Collapsed;
                Student.Visibility = Visibility.Collapsed;
            }
            else if (MainWindow.roleName == "teacher")
            {
                Logs.Visibility = Visibility.Collapsed;
                Teacher.Visibility = Visibility.Collapsed;
            }
        }

        private enum State
        {
            students,
            teachers,
            objects,
            marks,
            logs,
            groups
        }


        private void group_student_Click(object sender, RoutedEventArgs e)
        {
            NpgsqlCommand command = new NpgsqlCommand("Select * from group_student;", MainWindow.connect);
            command.ExecuteNonQuery();
            DataSet set = new DataSet();
            NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(command);
            (new NpgsqlDataAdapter(command)).Fill(set);

            DTable.ItemsSource = set.Tables[0].DefaultView;

            DTable.Columns[0].Header = "Группа";
            DTable.Columns[1].Header = "Куратор";

            // Так скрывать.
            DTable.Columns[0].Visibility = Visibility.Hidden;

            //AddButton.Visibility = Visibility.Hidden;
            DeleteObject.Visibility = Visibility.Hidden;

            AddStudent.Visibility = Visibility.Hidden;

            state = State.groups;
        }


        private void Student_Click(object sender, RoutedEventArgs e)
        {
            NpgsqlCommand command = new NpgsqlCommand("Select * from student;", MainWindow.connect);
            command.ExecuteNonQuery();
            DataSet set = new DataSet();
            NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(command);
            (new NpgsqlDataAdapter(command)).Fill(set);
            
            DTable.ItemsSource = set.Tables[0].DefaultView;

            DTable.Columns[0].Header = "Номер студента";
            DTable.Columns[1].Header = "Номер группы";
            DTable.Columns[2].Header = "Фамилия";
            DTable.Columns[3].Header = "Имя";
            DTable.Columns[4].Header = "Дата рождения";
            DTable.Columns[5].Header = "Курс";

            DTable.Columns[0].Visibility = Visibility.Hidden;

            //AddButton.Visibility = Visibility.Hidden;
            DeleteObject.Visibility = Visibility.Visible;
            AddStudent.Visibility = Visibility.Hidden; 
            state = State.students;
        }

        private void Object_Click(object sender, RoutedEventArgs e)
        {
            NpgsqlCommand command = new NpgsqlCommand("Select * from object;", MainWindow.connect);
            command.ExecuteNonQuery();
            DataSet set = new DataSet();
            NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(command);
            (new NpgsqlDataAdapter(command)).Fill(set);

            // Раскрыть
            DTable.ItemsSource = set.Tables[0].DefaultView;

            DTable.Columns[0].Header = "Номер предмета";
            DTable.Columns[1].Header = "Предмет";
            DTable.Columns[2].Header = "Преподаватель";

            DTable.Columns[0].Visibility = Visibility.Hidden;
            // Не то закоментил ХЫХ !
            //AddButton.Visibility = Visibility.Visible;
            DeleteObject.Visibility = Visibility.Visible;
            AddStudent.Visibility = Visibility.Visible;
            state = State.objects;
        }

        private void Items_mark_Click(object sender, RoutedEventArgs e)
        {
            NpgsqlCommand command = new NpgsqlCommand("Select * from items_mark;", MainWindow.connect);
            command.ExecuteNonQuery();
            DataSet set = new DataSet();
            NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(command);
            (new NpgsqlDataAdapter(command)).Fill(set);

            DTable.ItemsSource = set.Tables[0].DefaultView;

            DTable.Columns[0].Header = "Номер оценки";
            DTable.Columns[1].Header = "Номер студента";
            DTable.Columns[2].Header = "Номер предмета";
            DTable.Columns[3].Header = "Группа";
            DTable.Columns[4].Header = "Оценка за семестр";

            DTable.Columns[0].Visibility = Visibility.Hidden;
            DTable.Columns[1].Visibility = Visibility.Hidden;
            DTable.Columns[2].Visibility = Visibility.Hidden;

            //AddButton.Visibility = Visibility.Hidden;
            DeleteObject.Visibility = Visibility.Hidden;
            state = State.marks;
        }

        private void Teacher_Click(object sender, RoutedEventArgs e)
        {
            NpgsqlCommand command = new NpgsqlCommand("Select * from teacher;", MainWindow.connect);
            command.ExecuteNonQuery();
            DataSet set = new DataSet();
            NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(command);
            (new NpgsqlDataAdapter(command)).Fill(set);

            DTable.ItemsSource = set.Tables[0].DefaultView;

            DTable.Columns[0].Header = "Номер преподавателя";
            DTable.Columns[1].Header = "Фамилия";
            DTable.Columns[2].Header = "Имя";
            DTable.Columns[3].Header = "Образование";
            DTable.Columns[4].Header = "Опыт работы";

            DTable.Columns[0].Visibility = Visibility.Hidden;

            //AddButton.Visibility = Visibility.Hidden;
            DeleteObject.Visibility = Visibility.Hidden;
            state = State.teachers;
        }

        private void Logs_Click(object sender, RoutedEventArgs e)
        {
            NpgsqlCommand command = new NpgsqlCommand("Select * from logs;", MainWindow.connect);
            command.ExecuteNonQuery();
            DataSet set = new DataSet();
            NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(command);
            (new NpgsqlDataAdapter(command)).Fill(set);

            DTable.ItemsSource = set.Tables[0].DefaultView;

            DTable.Columns[0].Header = "Номер лога";
            DTable.Columns[1].Header = "Номер пользователя";
            DTable.Columns[2].Header = "";

            //AddButton.Visibility = Visibility.Hidden;
            DeleteObject.Visibility = Visibility.Hidden;
            state = State.logs;
        }
            


        private void Save(object sender, RoutedEventArgs e)
        {
            try
            {
                SaveFileDialog save = new SaveFileDialog();
                save.Filter = "Save file(*.xml)|*.xml";
                save.ShowDialog();
                if (save.FileName != "" && save.FileName != null)
                {
                    NpgsqlCommand command = new NpgsqlCommand("select database_to_xml(true,false,'')", MainWindow.connect);
                    File.WriteAllText(save.FileName, command.ExecuteScalar().ToString());
                    
                }
            }
            catch (Exception oshibochka) { MessageBox.Show("Ошибочка", oshibochka.Message); }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            (new CreateObject()).Show();
        }

        private void DeleteObject_Click(object sender, RoutedEventArgs e)
        {
            // Да
            switch (state)
            {
                case State.objects:
                    if (((DataRowView)DTable.SelectedValue).Row[0].ToString() != "-1") (new NpgsqlCommand("DELETE FROM OBJECT WHERE id_object = " + ((DataRowView)DTable.SelectedValue).Row[0].ToString() + ";", MainWindow.connect)).ExecuteNonQuery();
                    Object_Click(null, null);
                break;
                case State.students:
                    if (((DataRowView)DTable.SelectedValue).Row[0].ToString() != "-1") (new NpgsqlCommand("DELETE FROM STUDENT WHERE id_student = " + ((DataRowView)DTable.SelectedValue).Row[0].ToString() + ";", MainWindow.connect)).ExecuteNonQuery();
                    Student_Click(null, null);
                break;
            }
        }

        private void AddStudent_Click_1(object sender, RoutedEventArgs e)
        {
            switch (state)
            {
                case State.objects:
                    (new CreateObject()).Show();
                    break;

                case State.students:
                    (new AddStudent()).Show();
                    break;

                    // и тд.
                    // Ну типо, все.
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            (new MainWindow()).Show();
            MainWindow.connect.Close();
            this.Close();
        }
    }

    public class GroupStudent
    {
        public string group;
        public string curator;
    }

    public class Student : INotifyPropertyChanged
    {
        public int id_student;
        public string Number_group;
        public string Surname;
        public string Name;
        public string Data_of_born;
        public string Curse;

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
    }

    public class Object
    {
        public int id_object;
        public string Name_object;
        public string Teacher;
    }

    public class Items_mark
    {
        public int id_mark;
        public int id_student;
        public int id_object;
        public string Number_group;
        public int Rating_semester;
    }

   public class Teacher
   {
        public int id_teacher;
        public string Surname;
        public string Name;
        public string Education;
        public int Work_expirence;
   }

    public class Logs
    {
        public int id_log;
        public int id_user;
        public string operation;
        public string time_change;
        public string table_name;
        public int id_change;
        public string changes;
    }


    public class StudentListView : INotifyPropertyChanged
    {
        public ObservableCollection<Student> Students { get; set; }
        public StudentListView(NpgsqlCommand command)
        {

            using (NpgsqlDataReader reader = command.ExecuteReader())
            {
                Student student = new Student();
                Students = new ObservableCollection<Student>();
                while (reader.Read())
                {
                    student.id_student = reader.GetInt32(0);
                    student.Number_group = reader.GetString(1);
                    student.Surname = reader.GetString(2);
                    student.Name = reader.GetString(3);
                    student.Data_of_born = reader.GetDate(4).ToString();
                    student.Curse = reader.GetString(5);

                    Students.Add(student);
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
    }
}
