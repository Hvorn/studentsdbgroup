﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Npgsql;

namespace Group_student.Form
{
    /// <summary>
    /// Логика взаимодействия для Registrac.xaml
    /// </summary>
    public partial class Registrac : Window
    {
        public Registrac()
        {
            InitializeComponent();
            StudentFlag.IsChecked = true;
            ComboBoxSelector();
        }

        private void ComboBoxSelector()
        {
            using (NpgsqlDataReader reader = (new NpgsqlCommand("SELECT Number_group FROM group_student;", MainWindow.connect)).ExecuteReader())
            {
                while(reader.Read())
                {
                    NumberGroupInp.Items.Add(reader.GetString(0));
                }
            }
        }


        private void StudentFlag_Checked(object sender, RoutedEventArgs e)
        {
            // Выключаем поля преподавателей.
            Work_expirence_Inp.Visibility = Visibility.Hidden;
            EducationInp.Visibility = Visibility.Hidden;
            Born_Data.Visibility = Visibility.Visible;
            Nimber.Visibility = Visibility.Visible;
            Exp.Visibility = Visibility.Hidden;
            Obraz.Visibility = Visibility.Hidden;
            NumberGroupInp.Visibility = Visibility.Visible;
            Data_of_born_Inp.Visibility = Visibility.Visible;
            
        }

        private void TeacherFlag_Checked(object sender, RoutedEventArgs e)
        {

            Work_expirence_Inp.Visibility = Visibility.Visible;
            EducationInp.Visibility = Visibility.Visible;
            NumberGroupInp.Visibility = Visibility.Hidden;
            Data_of_born_Inp.Visibility = Visibility.Hidden;
            Exp.Visibility = Visibility.Visible;
            Obraz.Visibility = Visibility.Visible;
            Born_Data.Visibility = Visibility.Hidden;
            Nimber.Visibility = Visibility.Hidden;
        }

        private void Registrac_Click(object sender, RoutedEventArgs e)
        {
            NpgsqlCommand regist = new NpgsqlCommand();

            if ((bool)StudentFlag.IsChecked)
            {
                
                if (Work_expirence_Inp.Text == "" && EducationInp.Text == "") regist = new NpgsqlCommand("select AddStudent('" + NumberGroupInp.Text + "','" + Surname.Text + "','" + Names.Text + "','" + Data_of_born_Inp.Text + "','" + NumberGroupInp.Text[2] + "','" + Login.Text + "','" + Password.Password + "');", MainWindow.connect);

            }
            else if((bool)TeacherFlag.IsChecked)
            {
                if (NumberGroupInp.Text == "" && Data_of_born_Inp.Text == "") regist = new NpgsqlCommand("select AddTeacher('" + Surname.Text + "','" + Names.Text + "','" + EducationInp.Text + "'," + Work_expirence_Inp.Text + ",'" + Login.Text + "','" + Password.Password + "');", MainWindow.connect);

            }

            regist.ExecuteNonQuery();
            ClearFilds();
        }

        private void ClearFilds()
        {
            Work_expirence_Inp.Text = "";
            EducationInp.Text = "";
            Names.Text = "";
            Surname.Text = "";
            Data_of_born_Inp.Text = "";
            Login.Text = "";
            Password.Password = "";


            MessageBox.Show("Регистрация прошла успешно!");
        }
    }
}
